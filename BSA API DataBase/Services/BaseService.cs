﻿using BSA_API_DataBase.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSA_API_DataBase.Services
{
    public class BaseService
    {
        private readonly HttpClient _client;
        private string baseUrl = "https://bsa-dotnet.azurewebsites.net/api/";

        public BaseService(HttpClient client)
        {
            _client = client;
        }

        private async Task<List<T>> GetDataCollectionAsync<T>(string endpoint) where T : class
        {
            return JsonConvert.DeserializeObject<List<T>>(
                await _client.GetStringAsync(baseUrl + endpoint));
        }
        public async Task<List<User>> GetDataHierarchyAsync()
        {
            var users = await GetDataCollectionAsync<User>("Users");
            var projects = await GetDataCollectionAsync<Project>("Projects");
            var tasks = await GetDataCollectionAsync<Models.Task>("Tasks");
            var teams = await GetDataCollectionAsync<Team>("Teams");

            /* #region Binding collections
             var dataHierarchy = (from project in projects
                                  join task in tasks on project.Id equals task.ProjectId into taskGroup
                                  join team in teams on project.TeamId equals team.Id into teamGroup
                                  join user in users on project.AuthorId equals user.Id into authorGroup
                                  select new Project
                                  {
                                      Id = project.Id,
                                      AuthorId = project.AuthorId,
                                      TeamId = project.TeamId,
                                      Name = project.Name,
                                      Deadline = project.Deadline,
                                      Description = project.Description,
                                      CreatedAt = project.CreatedAt,

                                      Tasks = (from innerProjectTask in taskGroup
                                               join user in users on innerProjectTask.PerformerId equals user.Id into perfomerTaskGroup
                                               select new Models.Task
                                               {
                                                   Id = innerProjectTask.Id,
                                                   Name = innerProjectTask.Name,
                                                   Description = innerProjectTask.Description,
                                                   CreatedAt = innerProjectTask.CreatedAt,
                                                   FinishedAt = innerProjectTask.FinishedAt,
                                                   PerformerId = innerProjectTask.PerformerId,
                                                   ProjectId = innerProjectTask.ProjectId,
                                                   State = innerProjectTask.State,

                                                   Performers = (from innerPerfomer in perfomerTaskGroup
                                                                 select new User
                                                                 {
                                                                     Id = innerPerfomer.Id,
                                                                     FirstName = innerPerfomer.FirstName,
                                                                     LastName = innerPerfomer.LastName,
                                                                     Email = innerPerfomer.Email,
                                                                     TeamId = innerPerfomer.TeamId,
                                                                     RegisteredAt = innerPerfomer.RegisteredAt,
                                                                     BirthDay = innerPerfomer.BirthDay,

                                                                 }).ToList(),

                                               }).ToList(),


                                      Author = (from innerAuthor in authorGroup
                                                select new User
                                                {
                                                    Id = innerAuthor.Id,
                                                    FirstName = innerAuthor.FirstName,
                                                    LastName = innerAuthor.LastName,
                                                    Email = innerAuthor.Email,
                                                    TeamId = innerAuthor.TeamId,
                                                    RegisteredAt = innerAuthor.RegisteredAt,
                                                    BirthDay = innerAuthor.BirthDay,

                                                }).ToList(),

                                      Teams = (from innerProjectTeam in teamGroup
                                               select new Team
                                               {
                                                   Id = innerProjectTeam.Id,
                                                   Name = innerProjectTeam.Name,
                                                   CreatedAt = innerProjectTeam.CreatedAt,
                                               }).ToList()

                                  }).ToList();*/


            #region Binding collections
            var dataHierarchy = (from user in users
                                 join project in projects on user.Id equals project.AuthorId into projectGroup
                                 join task in tasks on user.Id equals task.PerformerId into taskGroup
                                 join team in teams on user.TeamId equals team.Id into userTeamGroup

                                 select new User
                                 {
                                     Id = user.Id,
                                     TeamId = user.TeamId,
                                     FirstName = user.FirstName,
                                     LastName = user.LastName,
                                     Email = user.Email,
                                     RegisteredAt = user.RegisteredAt,
                                     BirthDay = user.BirthDay,



                                     Teams = (from innerUserTeams in userTeamGroup
                                              select new Team
                                              {
                                                  Id = innerUserTeams.Id,
                                                  Name = innerUserTeams.Name,
                                                  CreatedAt = innerUserTeams.CreatedAt
                                              }).ToList(),
                                     Tasks = (from innerTask in taskGroup
                                              select new Models.Task
                                              {
                                                  Id = innerTask.Id,
                                                  ProjectId = innerTask.ProjectId,
                                                  PerformerId = innerTask.PerformerId,
                                                  Name = innerTask.Name,
                                                  Description = innerTask.Description,
                                                  State = innerTask.State,
                                                  CreatedAt = innerTask.CreatedAt,
                                                  FinishedAt = innerTask.FinishedAt

                                              }).ToList(),

                                     Projects = (from innerProject in projectGroup
                                                 join task in tasks on innerProject.Id equals task.ProjectId into projectTaskGroup
                                                 join team in teams on innerProject.TeamId equals team.Id into projectTeamGroup

                                                 select new Project
                                                 {
                                                     Id = innerProject.Id,
                                                     AuthorId = innerProject.AuthorId,
                                                     TeamId = innerProject.TeamId,
                                                     Name = innerProject.Name,
                                                     Description = innerProject.Description,
                                                     Deadline = innerProject.Deadline,
                                                     CreatedAt = innerProject.CreatedAt,

                                                     Teams = (from innerProjectTeam in projectTeamGroup
                                                              select new Team
                                                              {
                                                                  Id = innerProjectTeam.Id,
                                                                  Name = innerProjectTeam.Name,
                                                                  CreatedAt = innerProjectTeam.CreatedAt,

                                                              }).ToList(),


                                                     Tasks = (from innerProjectTask in projectTaskGroup
                                                              select new Models.Task
                                                              {
                                                                  Id = innerProjectTask.Id,
                                                                  ProjectId = innerProjectTask.ProjectId,
                                                                  PerformerId = innerProjectTask.PerformerId,
                                                                  Name = innerProjectTask.Name,
                                                                  Description = innerProjectTask.Description,
                                                                  State = innerProjectTask.State,
                                                                  CreatedAt = innerProjectTask.CreatedAt,
                                                                  FinishedAt = innerProjectTask.FinishedAt

                                                              }).ToList()


                                                 }).ToList()
                                 }).ToList();
            #endregion


            return dataHierarchy;

        }
    }
}
