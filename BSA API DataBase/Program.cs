﻿using BSA_API_DataBase.Base;
using BSA_API_DataBase.Models;
using BSA_API_DataBase.Services;

namespace BSA_API_DataBase
{
   
    class Program
    {
        static void Main(string[] args)
        {
            ConsoleUi.Run().GetAwaiter().GetResult();
        }
    }
}