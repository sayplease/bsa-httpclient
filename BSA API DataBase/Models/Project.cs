﻿


namespace BSA_API_DataBase.Models
{
    public class Project
    {
        
        public int Id { get; set; }
        
        public int AuthorId { get; set; }
        
        public int TeamId { get; set; }
        
        public string Name { get; set; }
        
        public string? Description { get; set; }
        
        public DateTime Deadline { get; set; }
        
        public DateTime CreatedAt { get; set; }

        

        public List<Task> Tasks { get; set; }

        public List<Team> Teams { get; set; }

        public List<User> Author { get; set; }

        
    }
}
