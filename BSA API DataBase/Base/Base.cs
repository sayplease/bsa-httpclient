﻿using BSA_API_DataBase.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;


namespace BSA_API_DataBase.Base
{
    public class BaseQueries
    {
        private readonly List<User> _baseHierarchy;
        public BaseQueries(List<User> baseHierarchy)
        {
            _baseHierarchy = baseHierarchy;
        }

        public List<(Project Project, int Tasks)> GetTasksFromUser(int userId)
        {
            return _baseHierarchy
                .Where(u => u.Tasks.All(t => t.PerformerId == userId))
                .SelectMany(x => x.Projects)
                .Select(p => (Project: p, TaskCount: p.Tasks.Count))
                .ToList();
        }


        public List<Models.Task> GetTaskWithName(int userId)
        {
            return _baseHierarchy
                .Where(u => u.Tasks.All(t => t.PerformerId == userId))
                .SelectMany(_x => _x.Tasks.Where(y => y.Name.Length < 45))
                .ToList();
        }
        public List<Models.Task> GetTaskWithIdandName(int userId)
        {
            return _baseHierarchy
                .Where(u => u.Tasks.All(t => t.PerformerId == userId))
                .SelectMany(_x => _x.Tasks.Where(t => t.FinishedAt > DateTime.UtcNow))
                .Where(t => t.State == TaskState.Done)
                .SelectMany(x => x.Tasks)

                .ToList();
        }

    }
    
}
